import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import net.automatalib.automata.fsa.FiniteStateAcceptor;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.collections.CollectionsUtil;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DumpRFSAEQOracle<A extends FiniteStateAcceptor<Integer, I>, I> extends RandomWordsEQOracle<A, I, Boolean> {
    private final Random random;
    private final int minLength;
    private final int maxLength;
    private final int maxTests;
    CompactNFA<I> target;

    public DumpRFSAEQOracle(CompactNFA<I> target, MembershipOracle<I, Boolean> mqOracle, int minLength, int maxLength, int maxTests) {
        this(target, mqOracle, minLength, maxLength, maxTests, new Random(), 1);
    }

    public DumpRFSAEQOracle(CompactNFA<I> target, MembershipOracle<I, Boolean> mqOracle, int minLength, int maxLength, int maxTests, Random random) {
        this(target, mqOracle, minLength, maxLength, maxTests, random, 1);
    }

    public DumpRFSAEQOracle(CompactNFA<I> target, MembershipOracle<I, Boolean> mqOracle, int minLength, int maxLength, int maxTests, Random random, int batchSize) {
        super(mqOracle, minLength, maxLength, maxTests, random, batchSize);

        this.target = target;
        this.random = random;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.maxTests = maxTests;
    }

    /**
     * after the given number of random words, equivalence of automata is used to find counterexamples
     */
    @Nullable
    @Override
    public DefaultQuery<I, Boolean> findCounterExample(A hypothesis, Collection<? extends I> inputs) {
        long startTime = System.nanoTime()/1000;
        // Fail fast on empty inputs
        if (inputs.isEmpty()) {
            return null;
        }

        final Stream<Word<I>> testWordStream = generateOwnTestWordStream(hypothesis, inputs);
        final Stream<Word<I>> ceStream = testWordStream.map(word -> findCounterExampleElementwise(hypothesis, word));
        Word<I> a = ceStream.filter(Objects::nonNull).findFirst().orElse(null);
        System.out.print("random words: ");
        System.out.println(System.nanoTime()/1000-startTime);

        if (a == null) { // no counterexample using random words found
            startTime = System.nanoTime()/1000;
            Word<I> counterEx = AutomataEq.eqCounterex(target, hypothesis);
            System.out.println(System.nanoTime()/1000-startTime);

            if (counterEx == null) {
                return null;
            } else {
                return new DefaultQuery<I, Boolean>(counterEx, target.computeOutput(counterEx));
            }
        }
        return new DefaultQuery<I, Boolean>(a, target.computeOutput(a));
    }

    Word<I> findCounterExampleElementwise(A hypothesis, Word<I> word) {
        Set<Integer> st1 = target.getInitialStates();
        Set<Integer> st2 = hypothesis.getInitialStates();
        int i = 0;
        for (I c : word){
            if (target.isAccepting(st1) != hypothesis.isAccepting(st2)) {
                return word.subWord(0, i);
            }

            st1 = target.getSuccessors(st1, Word.fromSymbols(c));
            st2 = hypothesis.getSuccessors(st2, Word.fromSymbols(c));

            i++;
        }
        if(target.isAccepting(st1) != hypothesis.isAccepting(st2)) {
            return word;
        } else
            return null;
    }

    protected Stream<Word<I>> generateOwnTestWordStream(A hypothesis, Collection<? extends I> inputs) {

        final List<? extends I> symbolList = CollectionsUtil.randomAccessList(inputs);

        return IntStream.range(0, maxTests).parallel().mapToObj(x -> generateTestWord(symbolList, symbolList.size()));
    }

    private Word<I> generateTestWord(List<? extends I> symbolList, int numSyms) {

        final int length = minLength + random.nextInt((maxLength - minLength) + 1);
        final WordBuilder<I> result = new WordBuilder<>(length);

        for (int j = 0; j < length; ++j) {
            int symidx = random.nextInt(numSyms);
            I sym = symbolList.get(symidx);
            result.append(sym);
        }

        return result.toWord();
    }
}
