import net.automatalib.automata.fsa.DFA;
import net.automatalib.words.Word;
import de.learnlib.oracle.membership.SimulatorOracle.DFASimulatorOracle;

public class RevDFASimulatorOracle<I> extends DFASimulatorOracle<I> {

	public RevDFASimulatorOracle(DFA<?, I> dfa) {
		super(dfa);
	}

	public Boolean answerQuery(Word<I> prefix, Word<I> suffix) {
		return super.answerQuery(RevSimulatorOracle.reverseWord(suffix), RevSimulatorOracle.reverseWord(prefix));
	}
}
