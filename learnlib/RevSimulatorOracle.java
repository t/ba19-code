import de.learnlib.oracle.membership.SimulatorOracle;
import net.automatalib.automata.concepts.SuffixOutput;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import de.learnlib.oracle.membership.SimulatorOracle.DFASimulatorOracle;

public class RevSimulatorOracle<I, D> extends SimulatorOracle<I, D> {

	public RevSimulatorOracle(SuffixOutput<I, D> automaton) {
		super(automaton);
	}

	public D answerQuery(Word<I> prefix, Word<I> suffix) {
		return super.answerQuery(reverseWord(suffix), reverseWord(prefix));
	}

	public static <I> Word<I> reverseWord(Word<I> w){
		WordBuilder<I> wb = new WordBuilder<I>(w);
		wb.reverse();
		return wb.toWord();
	}
}
