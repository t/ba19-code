import com.google.common.collect.Lists;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.FSABuilder;
import net.automatalib.visualization.Visualization;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

// Sorry, not a general parser, just for these files

public class TimbukParser {
    public Set<GraphvizParser.Edge> edges;
    public Set<String> initial;
    public Set<String> accepting;
    public Set<String> inputs;

    public TimbukParser(Path filename) throws IOException {
        edges = new HashSet<>();
        initial = new HashSet<>();
        accepting = new HashSet<>();
        inputs = new HashSet<>();

        Scanner s = new Scanner(filename);
        while(s.hasNextLine()) {
            String line = s.nextLine();

            if (line.startsWith("Final States")){
                String[] parts = line.split(" ");
                accepting.addAll(Arrays.asList(parts).subList(2, parts.length));
            }

            if (line.contains(" -> ")) {
                String[] parts = line.split(" -> ");

                if ("x".equals(parts[0].trim())) {
                    initial.add(parts[1].trim());
                } else {
                    int sep = parts[0].indexOf("(");

                    String label = parts[0].substring(0, sep).trim();
                    inputs.add(label);
                    String from = parts[0].substring(sep+1, parts[0].length()-1);

                    edges.add(new GraphvizParser.Edge(from, parts[1].trim(), label));
                }
            }
        }
    }

    CompactNFA<String> createNFA() {
        List<String> inputList = Lists.newArrayList(inputs.iterator());
        Alphabet<String> alphabet = Alphabets.fromList(inputList);

        FSABuilder<?, String, CompactNFA<String>> builder = new FSABuilder<>(new CompactNFA<>(alphabet));

        initial.forEach(builder::withInitial);
        accepting.forEach(builder::withAccepting);

        for (GraphvizParser.Edge e : edges) {
            builder.from(e.from).on(e.label).to(e.to);
        }

        return builder.create();
    }

    public static void main(String[] args) throws IOException {
        TimbukParser parser = new TimbukParser(Paths.get(args[0]));

        CompactNFA<String> nfa = parser.createNFA();

        RFSALearnAndCompare<String> learnAndCompare = new RFSALearnAndCompare<>();
        learnAndCompare.setTarget(nfa);

        int maxTests = 2000000;
        System.out.println(maxTests);
        learnAndCompare.learn((int) (2*learnAndCompare.target.getStates().size()), maxTests);

        NFA<?, String> result = learnAndCompare.experiment.getFinalHypothesis();
        NFA<?, String> result2 = learnAndCompare.experiment2.getFinalHypothesis();

        Visualization.visualize(result, learnAndCompare.target.getInputAlphabet());
        Visualization.visualize(result2, learnAndCompare.target.getInputAlphabet());

        System.out.println(args[0] + "," + nfa.getStates().size() + "," + result.size() + "," + learnAndCompare.experiment.getRounds().getCount() + "," + learnAndCompare.mqOracle.getCounter().getCount() + "," + result2.size() + "," + learnAndCompare.experiment2.getRounds().getCount() + "," + learnAndCompare.mqOracle2.getCounter().getCount());
    }
}
