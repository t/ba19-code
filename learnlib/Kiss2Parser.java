import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.visualization.Visualization;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 *  It is *not* a general KISS2 parser, it only parses the things I needed for the mealy machines.
 */
public class Kiss2Parser extends GraphvizParser {

    Kiss2Parser(Path filename) throws IOException {
        super(filename);
        nodes = new HashSet<>();
        nodes2 = new HashSet<>();
        edges = new HashSet<>();

        Scanner s = new Scanner(filename);
        while(s.hasNextLine()){
            String line = s.nextLine();
            line = line.split("#",2)[0].trim();

            if(line.isEmpty() || line.startsWith("."))
                continue;

            if(line.matches("\\p{Graph}+\\s+\\p{Graph}+\\s+\\p{Graph}+\\s+\\p{Graph}+")){
                int i = 0;
                String input = line.split(" ")[i].trim();
                ++i;

                while (line.split(" ")[i].isEmpty())
                    ++i;
                String from = line.split(" ")[i].trim();
                ++i;

                while (line.split(" ")[i].isEmpty())
                    ++i;
                String to = line.split(" ")[i].trim();
                ++i;

                while (line.split(" ")[i].isEmpty())
                    ++i;
                String out = line.split(" ")[i].trim();

                edges.add(new Edge(from, to, input + "/" + out));
                nodes2.add(from);
                nodes2.add(to);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Kiss2Parser parser = new Kiss2Parser(Paths.get(args[0]));
        CompactDFA<String> dfa = parser.createDFAfromMealy(Collections.singletonList("11"), args[1]);

        Visualization.visualize(dfa);

        CompactNFA<String> nfa = FSAUtil.convert(dfa);

        RFSALearnAndCompare<String> learnAndCompare = new RFSALearnAndCompare<>();
        learnAndCompare.setTarget(nfa);

        learnAndCompare.learn((int) (1.5*learnAndCompare.target.getStates().size()), 200000);

        NFA<?, String> result = learnAndCompare.experiment.getFinalHypothesis();
        NFA<?, String> result2 = learnAndCompare.experiment2.getFinalHypothesis();

        Visualization.visualize(result, learnAndCompare.target.getInputAlphabet());
        Visualization.visualize(result2, learnAndCompare.target.getInputAlphabet());

	System.out.println(args[0] + "," + nfa.getStates().size() + "," + result.size() + "," + learnAndCompare.experiment.getRounds().getCount() + "," + learnAndCompare.mqOracle.getCounter().getCount() + "," + result2.size() + "," + learnAndCompare.experiment2.getRounds().getCount() + "," + learnAndCompare.mqOracle2.getCounter().getCount());

    }
}
