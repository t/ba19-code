import de.learnlib.algorithms.nlstar.NLStarLearner;
import de.learnlib.algorithms.nlstar.NLStarLearnerBuilder;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.membership.SimulatorOracle;
import de.learnlib.util.Experiment;
import net.automatalib.automata.fsa.MutableNFA;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.builders.DFABuilder;
import net.automatalib.util.automata.builders.FSABuilder;
import net.automatalib.visualization.Visualization;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

import java.util.Random;

public class RandomNFAGen {

    Alphabet<Integer> alphabet;
    int state_count;
    Random rnd_gen;

    public RandomNFAGen(int state_count){
        this.state_count = state_count;
        alphabet = Alphabets.integers(0, 1);
        rnd_gen = new Random();
    }

    public CompactNFA<Integer> get(){
        FSABuilder<Integer, Integer, CompactNFA<Integer>> builder = new FSABuilder<>(new CompactNFA<>(alphabet))
                .withInitial(0);

        int transistion_count = rnd_gen.nextInt(state_count * alphabet.size());

        for (int i = 0; i < transistion_count; i++) {
            builder.from(rnd_gen.nextInt(state_count))
                    .on(rnd_gen.nextInt(alphabet.size()))
                    .to(rnd_gen.nextInt(state_count));
        }

        int accepting_count = rnd_gen.nextInt(state_count);

        for (int i = 0; i < accepting_count; i++) {
            builder.withAccepting(rnd_gen.nextInt(state_count));
        }

        return builder.create();
    }

    public static void main(String[] args) {
        RandomNFAGen c = new RandomNFAGen(10);
        RFSALearnAndCompare<Integer> learnAndCompare = new RFSALearnAndCompare<>();
        while (true) {
            learnAndCompare.setTarget(c.get());

            RFSALearnAndCompare.TriPredicate<CompactNFA<Integer>, Experiment<NFA<?,Integer>>, Experiment<NFA<?, Integer>>> comparator = (target, experiment, experiment2) -> experiment.getFinalHypothesis().size() > 2 || Math.abs(experiment.getFinalHypothesis().size() - experiment2.getFinalHypothesis().size()) > 3;

            if (learnAndCompare.learnAndCompare(comparator,75, 5000)) {
                System.out.print(learnAndCompare.experiment.getFinalHypothesis().size() + ",");
                System.out.println(learnAndCompare.experiment2.getFinalHypothesis().size());
                Visualization.visualize(learnAndCompare.target, learnAndCompare.target.getInputAlphabet());
                Visualization.visualize(learnAndCompare.experiment.getFinalHypothesis(), learnAndCompare.target.getInputAlphabet());
                Visualization.visualize(learnAndCompare.experiment2.getFinalHypothesis(), learnAndCompare.target.getInputAlphabet());
            }
        }
    }
}
