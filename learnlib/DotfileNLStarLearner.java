import de.learnlib.algorithms.nlstar.NLStarLearner;
import de.learnlib.algorithms.nlstar.NLStarLearnerBuilder;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.filter.statistic.oracle.CounterOracle;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.membership.SimulatorOracle;
import de.learnlib.util.Experiment;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.visualization.Visualization;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class DotfileNLStarLearner {
    CompactDFA<String> target;
    NFA<?, String> result, result2;
    Experiment experiment, experiment2;
    CounterOracle.DFACounterOracle<String> mqOracle, mqOracle2;

    DotfileNLStarLearner(Path path) throws IOException {
        GraphvizParser parser = new GraphvizParser(path);
        target = parser.createDFA(Arrays.asList("0", "1K", "2K", "3K"));

        CompactNFA<String> reverseTarget = FSAUtil.reverse(target);

        MembershipOracle.DFAMembershipOracle<String> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
        MembershipOracle.DFAMembershipOracle<String> sul2 = new RevDFASimulatorOracle<>(target);

        // oracle for counting queries wraps SUL
        mqOracle = new CounterOracle.DFACounterOracle<>(sul, "membership queries learning");
        mqOracle2 = new CounterOracle.DFACounterOracle<>(sul2, "membership queries learning");

        CounterOracle.DFACounterOracle<String> mqOracle_2 = new CounterOracle.DFACounterOracle<>(sul, "membership queries testing");
        CounterOracle.DFACounterOracle<String> mqOracle2_2 = new CounterOracle.DFACounterOracle<>(sul2, "membership queries testing");

        // construct L* instances
        NLStarLearner<String> nlstar =
                new NLStarLearnerBuilder<String>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle) // membership oracle
                        .create();
        NLStarLearner<String> nlstar2 =
                new NLStarLearnerBuilder<String>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle2) // reverse membership oracle
                        .create();

        DumpRFSAEQOracle<NFA<Integer, String>, String> eqOracle = new DumpRFSAEQOracle<>(FSAUtil.convert(target), mqOracle_2, 1, 16, 5000);
        DumpRFSAEQOracle<NFA<Integer, String>, String> eqOracle2 = new DumpRFSAEQOracle<>(reverseTarget, mqOracle2_2, 1, 16, 5000);

        experiment = new Experiment(nlstar, eqOracle, target.getInputAlphabet());
        experiment2 = new Experiment(nlstar2, eqOracle2, target.getInputAlphabet());

        // turn on time profiling
        experiment.setProfile(true);
        experiment2.setProfile(true);

        // enable logging of models
        experiment.setLogModels(true);
        experiment2.setLogModels(true);

        RFSALearnAndCompare.LearnThread thread1 = new RFSALearnAndCompare.LearnThread(experiment);
        RFSALearnAndCompare.LearnThread thread2 = new RFSALearnAndCompare.LearnThread(experiment2);

        // run experiment
        thread1.start();
        thread2.start();

        try {
            thread1.join();

            // get learned model
            result = (NFA<?, String>) experiment.getFinalHypothesis();

            thread2.join();

            // get learned model
            result2 = (NFA<?, String>) experiment2.getFinalHypothesis();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        DotfileNLStarLearner experiments = new DotfileNLStarLearner(Paths.get(args[0]));

        System.out.println(args[0] + "," + experiments.target.getStates().size() + "," + experiments.result.size() + "," + experiments.experiment.getRounds().getCount() + "," + experiments.mqOracle.getCounter().getCount() + "," + experiments.result2.size() + "," + experiments.experiment2.getRounds().getCount() + "," + experiments.mqOracle2.getCounter().getCount());

        Visualization.visualize(experiments.target, experiments.target.getInputAlphabet());

        System.out.println("States: " + experiments.result.size());
        System.out.println("States: " + experiments.result2.size());

        Visualization.visualize(experiments.result, experiments.target.getInputAlphabet());
        Visualization.visualize(experiments.result2, experiments.target.getInputAlphabet());
    }
}
