import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.FSABuilder;

public class FSAUtil<A extends CompactNFA<I>, S, I> {
    public static <I> CompactNFA<I> reverse(CompactNFA<I> original) {
        FSABuilder<Integer, I, CompactNFA<I>> builder = new FSABuilder<>(new CompactNFA<>(original.getInputAlphabet(), original.getStates().size()));

        for (int a : original.getStates()) {
            if (original.isAccepting(a))
                builder.withInitial(a);

            for (I i : original.getInputAlphabet())
                for (int c : original.getTransitions(a, i))
                    builder.from(c).on(i).to(a);
        }

        original.getInitialStates().forEach(builder::withAccepting);
        return builder.create();
    }

    public static <I> CompactNFA<I> reverse(CompactDFA<I> original) {
        return reverse(convert(original));
    }
    
    public static <I> CompactNFA<I> convert(CompactDFA<I> original) {
        FSABuilder<Integer, I, CompactNFA<I>> builder = new FSABuilder<>(new CompactNFA<>(original.getInputAlphabet(), original.getStates().size()));

        original.getInitialStates().forEach(builder::withInitial);

        for (int a : original.getStates()) {
            if (original.isAccepting(a))
                builder.withAccepting(a);

            for (I i : original.getInputAlphabet())
                for (int c : original.getTransitions(a, i))
                    builder.from(a).on(i).to(c);

        }

        return builder.create();
    }
}
