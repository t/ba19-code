import de.learnlib.algorithms.nlstar.NLStarLearner;
import de.learnlib.algorithms.nlstar.NLStarLearnerBuilder;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.filter.statistic.oracle.CounterOracle;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.membership.SimulatorOracle;
import de.learnlib.util.Experiment;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.visualization.Visualization;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public class MealyDotfileNLStarLearner {
    CompactDFA<String> target;
    NFA<?, String> result, result2;

    MealyDotfileNLStarLearner(Path path, String accepting_output) throws IOException {
        GraphvizParser parser = new GraphvizParser(path);
        target = parser.createDFAfromMealy(Collections.singletonList(accepting_output));

        MembershipOracle.DFAMembershipOracle<String> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
        MembershipOracle.DFAMembershipOracle<String> sul2 = new RevDFASimulatorOracle<>(target);

        // oracle for counting queries wraps SUL
        CounterOracle.DFACounterOracle<String> mqOracle = new CounterOracle.DFACounterOracle<>(sul, "membership queries learning");
        CounterOracle.DFACounterOracle<String> mqOracle2 = new CounterOracle.DFACounterOracle<>(sul2, "membership queries learning");

        CounterOracle.DFACounterOracle<String> mqOracle_2 = new CounterOracle.DFACounterOracle<>(sul, "membership queries testing");
        CounterOracle.DFACounterOracle<String> mqOracle2_2 = new CounterOracle.DFACounterOracle<>(sul2, "membership queries testing");

        // construct L* instances
        NLStarLearner<String> nlstar =
                new NLStarLearnerBuilder<String>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle) // membership oracle
                        .create();
        NLStarLearner<String> nlstar2 =
                new NLStarLearnerBuilder<String>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle2) // reverse membership oracle
                        .create();

        RandomWordsEQOracle<NFA<?, String>, String, Boolean> eqOracle = new RandomWordsEQOracle<>(mqOracle_2, 0, 16, 5000);
        RandomWordsEQOracle<NFA<?, String>, String, Boolean> eqOracle2 = new RandomWordsEQOracle<>(mqOracle2_2, 0, 16, 5000);

        Experiment<NFA<?,String>> experiment = new Experiment<>(nlstar, eqOracle, target.getInputAlphabet());
        Experiment<NFA<?,String>> experiment2 = new Experiment<>(nlstar2, eqOracle2, target.getInputAlphabet());

        // turn on time profiling
        experiment.setProfile(true);
        experiment2.setProfile(true);

        // enable logging of models
        experiment.setLogModels(true);
        experiment2.setLogModels(true);

        // run experiment
        experiment.run();

        // get learned model
        result = experiment.getFinalHypothesis();

        experiment2.run();

        // get learned model
        result2 = experiment2.getFinalHypothesis();


    }

    public static void main(String[] args) throws IOException {
        for (int i = 1; i<=1; i++) {
            String file = "river.flat_0_" + i + ".dot";
            MealyDotfileNLStarLearner experiments = new MealyDotfileNLStarLearner(Paths.get(file), "coffee!");

            //Visualization.visualize(experiments.target, experiments.target.getInputAlphabet());

            System.out.println(file + "," + experiments.result.size() + "," + experiments.result2.size());

            //Visualization.visualize(experiments.result, experiments.target.getInputAlphabet());
            //Visualization.visualize(experiments.result2, experiments.target.getInputAlphabet());
        }

    }
}
