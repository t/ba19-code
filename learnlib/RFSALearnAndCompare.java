import de.learnlib.algorithms.nlstar.NLStarLearner;
import de.learnlib.algorithms.nlstar.NLStarLearnerBuilder;
import de.learnlib.filter.statistic.oracle.CounterOracle;
import de.learnlib.oracle.membership.SimulatorOracle;
import de.learnlib.util.Experiment;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;

public class RFSALearnAndCompare<I> {

    CompactNFA<I> target;
    Experiment<NFA<?, I>> experiment, experiment2;
    CounterOracle<I, Boolean> mqOracle, mqOracle2;

    public RFSALearnAndCompare() {

    }

    public RFSALearnAndCompare(CompactNFA<I> target) {
        this();
        setTarget(target);
    }

    public void setTarget(CompactNFA<I> target) {
        this.target = target;
    }

    public void learn(int maxLength, int maxTests) {
        SimulatorOracle<I, Boolean> sul = new SimulatorOracle<>(target);
        SimulatorOracle<I, Boolean> sul2 = new RevSimulatorOracle<>(target);

        CompactNFA<I> reverseTarget = FSAUtil.reverse(target);

        mqOracle = new CounterOracle<>(sul, "membership queries learning");
        mqOracle2 = new CounterOracle<>(sul2, "membership queries learning");

        CounterOracle<I, Boolean> mqOracle_2 = new CounterOracle<>(sul, "membership queries testing");
        CounterOracle<I, Boolean> mqOracle2_2 = new CounterOracle<>(sul2, "membership queries testing");

        NLStarLearner<I> nlstar = new NLStarLearnerBuilder<I>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle) // membership oracle
                        .create();
        NLStarLearner<I> nlstar2 = new NLStarLearnerBuilder<I>().withAlphabet(target.getInputAlphabet()) // input alphabet
                        .withOracle(mqOracle2) // reverse membership oracle
                        .create();
        DumpRFSAEQOracle<NFA<Integer, I>, I> eqOracle = new DumpRFSAEQOracle<>(target, mqOracle_2, 1, maxLength, maxTests);
        DumpRFSAEQOracle<NFA<Integer, I>, I> eqOracle2 = new DumpRFSAEQOracle<>(reverseTarget, mqOracle2_2, 1, maxLength, maxTests);

        experiment = new Experiment(nlstar, eqOracle, target.getInputAlphabet()); //no type-checking here sry
        experiment2 = new Experiment(nlstar2, eqOracle2, target.getInputAlphabet());

        Thread expThread = new LearnThread(experiment);
        Thread expThread2 = new LearnThread(experiment2);

        expThread.start();
        expThread2.start();

        try {
            expThread.join();
            expThread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean learnAndCompare(TriPredicate<CompactNFA<I>, Experiment<NFA<?, I>>, Experiment<NFA<?, I>>> comparator, int maxLength, int maxTests) {
        learn(maxLength, maxTests);
        return comparator.apply(target, experiment, experiment2);
    }

    @FunctionalInterface
    interface TriPredicate<One, Two, Three> {
        public boolean apply(One one, Two two, Three three);
    }

    static class LearnThread extends Thread {
        Experiment experiment;

        LearnThread(Experiment experiment){
            this.experiment = experiment;
        }

        @Override
        public void run() {
            experiment.run();
        }
    }
}
