/* Copyright (C) 2013-2018 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.lang.reflect.Field;

import de.learnlib.algorithms.lstar.dfa.ClassicLStarDFA;
import de.learnlib.algorithms.lstar.dfa.ClassicLStarDFABuilder;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealy;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealyBuilder;
import de.learnlib.algorithms.nlstar.NLStarLearner;
import de.learnlib.algorithms.nlstar.NLStarLearnerBuilder;
import de.learnlib.algorithms.nlstar.ObservationTable;
import de.learnlib.api.oracle.MembershipOracle.DFAMembershipOracle;
import de.learnlib.filter.statistic.oracle.CounterOracle.DFACounterOracle;
import de.learnlib.oracle.equivalence.RandomWMethodEQOracle;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.equivalence.WMethodEQOracle.DFAWMethodEQOracle;
import de.learnlib.oracle.membership.SimulatorOracle.DFASimulatorOracle;
import de.learnlib.util.Experiment;
import de.learnlib.util.Experiment.DFAExperiment;
import de.learnlib.util.statistics.SimpleProfiler;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.visualization.Visualization;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

/**
 * This example shows the usage of a learning algorithm and an equivalence test as part of an experiment in order to
 * learn a simulated SUL (system under learning).
 *
 * @author falkhowar
 */
public final class Example1nlstar {

    private static final int EXPLORATION_DEPTH = 4;

    private Example1nlstar() {
        // prevent instantiation
    }

    public static void main(String[] args) throws IOException {

        // load DFA and alphabet
        CompactDFA<Character> target = constructSUL8();
        Alphabet<Character> inputs = target.getInputAlphabet();

        // construct a simulator membership query oracle
        // input  - Character (determined by example)
        DFAMembershipOracle<Character> sul = new DFASimulatorOracle<>(target);
        DFAMembershipOracle<Character> sul2 = new RevDFASimulatorOracle<>(target);

        // oracle for counting queries wraps SUL
        DFACounterOracle<Character> mqOracle = new DFACounterOracle<>(sul, "membership queries learning");
        DFACounterOracle<Character> mqOracle2 = new DFACounterOracle<>(sul2, "membership queries learning");

		DFACounterOracle<Character> mqOracle_2 = new DFACounterOracle<>(sul, "membership queries testing");
        DFACounterOracle<Character> mqOracle2_2 = new DFACounterOracle<>(sul2, "membership queries testing");

        // construct L* instances
        NLStarLearner<Character> nlstar =
                new NLStarLearnerBuilder<Character>().withAlphabet(inputs) // input alphabet
                                                     .withOracle(mqOracle) // membership oracle
                                                     .create();
        NLStarLearner<Character> nlstar2 =
                new NLStarLearnerBuilder<Character>().withAlphabet(inputs) // input alphabet
                                                     .withOracle(mqOracle2) // reverse membership oracle
                                                     .create();


        // construct
        RandomWordsEQOracle<NFA<?, Character>, Character, Boolean> eqOracle = new RandomWordsEQOracle<>(mqOracle_2, 0, 16, 5000);
        RandomWordsEQOracle<NFA<?, Character>, Character, Boolean> eqOracle2 = new RandomWordsEQOracle<>(mqOracle2_2, 0, 16, 5000);

        // construct a learning experiment from
        // the learning algorithm and the conformance test.
        // The experiment will execute the main loop of
        // active learning
        Experiment<NFA<?,Character>> experiment = new Experiment<>(nlstar, eqOracle, inputs);
        Experiment<NFA<?,Character>> experiment2 = new Experiment<>(nlstar2, eqOracle2, inputs);

        // turn on time profiling
        experiment.setProfile(true);
        experiment2.setProfile(true);

        // enable logging of models
        experiment.setLogModels(true);
        experiment2.setLogModels(true);

        // run experiment
        experiment.run();

        // get learned model
        NFA<?, Character> result = experiment.getFinalHypothesis();

        // report results
        System.out.println("-------------------------------------------------------");

        // profiling
        System.out.println(SimpleProfiler.getResults());

        // learning statistics
        System.out.println(experiment.getRounds().getSummary());
        System.out.println(mqOracle.getStatisticalData().getSummary());
        System.out.println(mqOracle_2.getStatisticalData().getSummary());

        // model statistics
        System.out.println("States: " + result.size());
        System.out.println("Sigma: " + inputs.size());

	    System.out.println();
        System.out.println("Model: ");
        GraphDOT.write(result, inputs, System.out); // may throw IOException!

        Visualization.visualize(result, inputs);

        System.out.println("-------------------------------------------------------");

        // run experiment
        experiment2.run();

        // get learned model
        NFA<?, Character> result2 = experiment2.getFinalHypothesis();

        // report results
        System.out.println("-------------------------------------------------------");

        // profiling
        System.out.println(SimpleProfiler.getResults());

        // learning statistics
        System.out.println(experiment2.getRounds().getSummary());
        System.out.println(mqOracle2.getStatisticalData().getSummary());
        System.out.println(mqOracle2_2.getStatisticalData().getSummary());

        // model statistics
        System.out.println("States: " + result2.size());
        System.out.println("Sigma: " + inputs.size());

        // show model
        System.out.println();
        System.out.println("Model: ");
        GraphDOT.write(result2, inputs, System.out); // may throw IOException!

        Visualization.visualize(result2, inputs);

        System.out.println("-------------------------------------------------------");


        System.out.println("Final observation table:");
        Field privateStringField = null;
        try {
            privateStringField = nlstar.getClass().getDeclaredField("table");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        privateStringField.setAccessible(true);
        try {
            ((ObservationTable<?>)privateStringField.get(nlstar)).getUpperPrimes().forEach(x->System.out.println(x.getPrefix()));

            ((ObservationTable<?>)privateStringField.get(nlstar2)).getUpperPrimes().forEach(x->System.out.println(x.getPrefix()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //new ObservationTableASCIIWriter<>().write(nlstar2.getObservationTable(), System.out);

        //OTUtils.displayHTMLInBrowser(lstar.getObservationTable());
        //OTUtils.displayHTMLInBrowser(lstar2.getObservationTable());
    }

    /**
     * creates example from Angluin's seminal paper.
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                    .on('a').to("q1")
                    .on('b').to("q2")
                .from("q1")
                    .on('a').to("q0")
                    .on('b').to("q3")
                .from("q2")
                    .on('a').to("q3")
                    .on('b').to("q0")
                .from("q3")
                    .on('a').to("q2")
                    .on('b').to("q1")
                .withAccepting("q0")
                .create();
        // @formatter:on
    }

    /**
     * creates example
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL2() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                    .on('a').to("q1")
                    .on('b').to("q2")
                .from("q1")
                    .on('a').to("q1")
                    .on('b').to("q1")
                .from("q2")
                    .on('a').to("q3")
                    .on('b').to("q1")
                .from("q3")
                    .on('a').to("q3")
                    .on('b').to("q1")
                .withAccepting("q3")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

    /**
     * creates example
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL3() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                    .on('a').to("q0")
                    .on('b').to("q1")
                .from("q1")
                    .on('a').to("q2")
                    .on('b').to("q3")
                .from("q2")
                    .on('a').to("q4")
                    .on('b').to("q0")
                .from("q3")
                    .on('a').to("q1")
                    .on('b').to("q2")
                .from("q4")
                    .on('a').to("q3")
                    .on('b').to("q4")
                .withAccepting("q0")
                .create();
        // @formatter:on
    }

    /**
     * creates example
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL4() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                    .on('a').to("q1")
                    .on('b').to("q1")
                .from("q1")
                    .on('a').to("q2")
                    .on('b').to("q0")
                .from("q2")
                    .on('a').to("q2")
                    .on('b').to("q3")
                .from("q3")
                    .on('a').to("q0")
                    .on('b').to("q2")
                .withAccepting("q1")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

    /**
     * creates example
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL5() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                    .on('a').to("q1")
                    .on('b').to("q1")
                .from("q1")
                    .on('a').to("q2")
                    .on('b').to("q0")
                .from("q2")
                    .on('a').to("q3")
                    .on('b').to("q3")
                .from("q3")
                    .on('a').to("q0")
                    .on('b').to("q2")
                .withAccepting("q1")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

    private static CompactDFA<Character> constructSUL6() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                .on('a').to("q1")
                .on('b').to("q1")
                .from("q1")
                .on('a').to("q2")
                .on('b').to("q0")
                .from("q2")
                .on('a').to("q0")
                .on('b').to("q3")
                .from("q3")
                .on('a').to("q0")
                .on('b').to("q2")
                .withAccepting("q1")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

    private static CompactDFA<Character> constructSUL7() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                .on('a').to("q1")
                .on('b').to("q1")
                .from("q1")
                .on('a').to("q2")
                .on('b').to("q0")
                .from("q2")
                .on('a').to("q2")
                .on('b').to("q3")
                .from("q3")
                .on('a').to("q3")
                .on('b').to("q2")
                .withAccepting("q1")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

    private static CompactDFA<Character> constructSUL8() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // @formatter:off
        // create automaton
        return AutomatonBuilders.newDFA(sigma)
                .withInitial("q0")
                .from("q0")
                .on('a').to("q1")
                .on('b').to("q4")
                .from("q4")
                .on('a').to("q5")
                .from("q5")
                .on('b').to("q1")
                .from("q1")
                .on('a').to("q2")
                .on('b').to("q3")
                .from("q2")
                .on('a').to("q2")
                .on('b').to("q1")
                .withAccepting("q1")
                .withAccepting("q2")
                .create();
        // @formatter:on
    }

}
